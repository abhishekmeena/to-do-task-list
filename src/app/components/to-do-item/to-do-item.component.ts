import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
@Component({
  selector: 'app-to-do-item',
  templateUrl: './to-do-item.component.html',
  styleUrls: ['./to-do-item.component.css']
})
export class ToDoItemComponent implements OnInit {
  @Input() task:any;
  @Output() updtTask= new EventEmitter();
  @Output() edtTask= new EventEmitter();
  @Output() delTask= new EventEmitter();
  constructor() {}
  editTask(value: any) {
    this.edtTask.emit(value);
  }
  updateTaskStatus(value: any) {
    this.updtTask.emit(value);
  }
  deleteTask(value: any) {
    if(confirm("Are you sure you want to delete this Task?"))
      this.delTask.emit(value);
  }
  ngOnInit() {
    // this.store.dispatch({ type: '[Todos Page] Load Todos' });
    // console.log(this.todos$.subscribe);
    
  }
}
