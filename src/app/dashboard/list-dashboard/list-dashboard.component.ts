import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { HttpServicesService } from 'src/app/services/http-services.service';
import { TodosService } from 'src/app/services/todos.service';
declare var $: any;

@Component({
  selector: 'app-list-dashboard',
  templateUrl: './list-dashboard.component.html',
  styleUrls: ['./list-dashboard.component.css']
})
export class ListDashboardComponent implements OnInit {
  todoListItems: Array<any> = [];
  todoList: Array<any> = [];
  selectedTask: any;
  selectedTaskItem: any;
  selectedTaskIndex: number = 0;
  todos$: Observable<TodosService[]> = this.store.select(state => state.data);
  taskForm: FormGroup;
  taskItemForm: FormGroup;
  constructor(private router: Router, private httpService: HttpServicesService, private store: Store<{ data: TodosService[] }>) {
    this.taskForm = new FormGroup({
      "id": new FormControl(''),
      'title': new FormControl('', Validators.required),
      'status': new FormControl(false)
    });
    this.taskItemForm = new FormGroup({
      "id": new FormControl(''),
      'title': new FormControl('', Validators.required),
      'todolist_id': new FormControl('', Validators.required),
      'status': new FormControl(false)
    });
  }
  logout() {
    localStorage.clear();
    this.httpService.auth_token = null;
    // this.httpService.userInfo=null;
    this.router.navigateByUrl("/account/login");
  }
  editTaskItem(taskItem: any) {
    console.log(taskItem);
    this.selectedTaskItem = taskItem;
    this.taskItemForm.patchValue(taskItem)
    $("#tastItemModal").modal('show');
  }
  addTaskItem() {
    this.taskItemForm.patchValue({ "todolist_id": this.todoList[this.selectedTaskIndex].id })
  }
  getTodoList() {
    let self = this
    this.httpService.get('todolists/index.json').subscribe((res: any) => {
      if (!res.error) {
        self.todoList = res.data;
        if (self.todoList[self.selectedTaskIndex])
          self.todoListItems = self.todoList[self.selectedTaskIndex].todolistitems;
      }
      else {
        self.httpService.showError(res.message);
        console.log(res);
      }
    },
      (err) => console.error(err));
  }
  UpdateTaskItem(item: any) {
    console.log(this.selectedTask,item);
    this.selectedTaskItem=item
    this.taskItemForm.patchValue(item)
    // this.selectedTask.id = item.todolist_id;
    this.saveTaskItem();
  }
  saveTask() {
    let self = this
    let reqData = this.taskForm.value;
    if (this.selectedTask?.id) {
      var reqUrl = "todolists/edit/"+this.selectedTask?.id+".json";
    }
    else {
      var reqUrl = "todolists/add.json";
    }
    this.httpService.post(reqUrl, reqData).subscribe((res: any) => {
      if (!res.error) {
        self.httpService.showSuccess(res.message);
        self.taskForm.reset();
        $(".close").click();
        self.getTodoList();
      }
      else {
        self.httpService.showError(res.message);
        console.log(res);
      }
    },
      (err) => console.error(err));
  }
  changeTaskItemsList(indx: number) {
    this.selectedTaskIndex = indx;
    this.todoListItems = this.todoList[this.selectedTaskIndex].todolistitems
  }
  saveTaskItem() {
    let self = this
    let reqData = this.taskItemForm.value;
    if (this.selectedTaskItem?.id) {
      var reqUrl = "todolistitems/edit/"+this.selectedTaskItem?.id+".json";
    }
    else {
      var reqUrl = "todolistitems/add.json";
    }
    this.httpService.post(reqUrl, reqData).subscribe((res: any) => {
      if (!res.error) {
        self.httpService.showSuccess(res.message);
        self.taskItemForm.reset();

        self.selectedTaskItem = null;
        $(".taskItemclose").click();
        self.getTodoList();
      }
      else {
        self.httpService.showError(res.message);
        console.log(res);
      }
    },
      (err) => console.error(err));
  }
  deleteItem(val: any) {
    let self = this
    this.httpService.post('todolistitems/delete/'+val.id+'.json', { id: val.id }).subscribe((res: any) => {
      if (!res.error) {
        self.getTodoList();
      }
      else {
        self.httpService.showError(res.message);
        console.log(res);
      }
    },
      (err) => console.error(err));
  }
  editItem(val: any) {
    console.log(val);
  }
  ngOnInit(): void {
    this.getTodoList()
    // this.store.dispatch({ type: '[Todos Page] Load Todos' });
  }

}
