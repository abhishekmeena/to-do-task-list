import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthService } from './services/auth.service';
import { LoaderComponent } from './components/loader/loader.component';
import { LoaderService } from './services/loader.service';
import { LoaderInterceptor } from './interceptors/loader.interceptor';
import { EffectsModule } from '@ngrx/effects';
// import { ToDoItemComponent } from './components/to-do-item/to-do-item.component';
import { StoreModule } from '@ngrx/store';
import { TodosEffects } from './effects/todos.effects';

@NgModule({
  declarations: [
    AppComponent,
    LoaderComponent,
    // ToDoItemComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    ToastrModule.forRoot(),
    EffectsModule.forRoot([TodosEffects]),
    StoreModule.forRoot({}, {})
  ],
  providers: [AuthService,LoaderService,{ provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
