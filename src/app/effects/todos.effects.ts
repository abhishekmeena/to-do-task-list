import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { TodosService } from '../services/todos.service';

@Injectable()
export class TodosEffects {

  loadTodos$ = createEffect(() => this.actions$.pipe(
    ofType('[Todos Page] Load Todos'),
    mergeMap(() => this.todosService.getAll()
      .pipe(
        map(todos => ({ type: '[Todos API] Todos Loaded Success', payload: todos })),
        catchError(() => EMPTY)
      ))
    )
  );

  constructor(
    private actions$: Actions,
    private todosService: TodosService
  ) {}
}