import { Injectable } from '@angular/core';
import {  Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate{
  constructor( private router: Router) {}  
  canActivate(next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree{  
      if (!this.gettoken()) {  
          this.router.navigateByUrl("/account/login");
      }  
      return !!this.gettoken();
  }  
  gettoken(){  
     return localStorage.getItem("userTocken");  
  } 
}
