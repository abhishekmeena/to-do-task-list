import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TodosService {
  constructor (private http: HttpClient) {

  }

  createAuthorizationHeader() {
    if (localStorage.getItem("userTocken")) {
      return new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem("userTocken")
      });
    }
    else {
      return new HttpHeaders({
        'Content-Type': 'application/json'
      });
    }
  }
  getAll() {
    return this.http.get(environment.baseUrl+'todolist/index.json', { headers: this.createAuthorizationHeader() });
  }
}
