import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpServicesService {
  public serviceBase: any;
  public auth_token: any;
  constructor(public http: HttpClient, private toastrService: ToastrService) {
    if (localStorage.getItem('userTocken')) {
      this.auth_token = localStorage.getItem('userTocken');
    }
    this.serviceBase = environment.baseUrl;
  }
  createAuthorizationHeader() {
    if (localStorage.getItem("userTocken")) {
      return new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem("userTocken")
      });
    }
    else {
      return new HttpHeaders({
        'Content-Type': 'application/json'
      });
    }
  }

  get(url: any) {
    return this.http.get(this.serviceBase + url, { headers: this.createAuthorizationHeader() });
  }
  post(url: any, data: any) {
    return this.http.post(this.serviceBase + url, data, { headers: this.createAuthorizationHeader() });
  }
  showSuccess(message: any) {
    this.toastrService.success(message);
  }
  showError(message: any) {
    this.toastrService.error(message);
  }
}
