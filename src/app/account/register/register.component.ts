import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpServicesService } from 'src/app/services/http-services.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  regForm: FormGroup;
  submitted = false;

  constructor(private router: Router,private httpService:HttpServicesService) {
    this.regForm = new FormGroup({
      'username': new FormControl('', Validators.required),
      'first_name': new FormControl('', Validators.required),
      'last_name': new FormControl(''),
      'gender': new FormControl(''),
      'email': new FormControl('', [Validators.required,Validators.pattern(/^([_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5}))|(\d{10})$/)]), 
      'mobile': new FormControl('',[Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]),
      'role_id': new FormControl('2',[Validators.required]),
      'status': new FormControl('1',[Validators.required]),
      
      'password': new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(40)
      ]),
      'cpassword': new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(40)
      ]),
      'terms': new FormControl(false, Validators.requiredTrue)
    });

  }
  get f(): { [key: string]: AbstractControl } {
    return this.regForm.controls;
  }

  onSubmit(): void {
    this.submitted = true;
    let reqData=this.regForm.value;
    if (this.regForm.invalid) {
      return;
    }
    else if(reqData.password!==reqData.cpassword){
      this.httpService.showError("Password and Confirm Password does not matched.")
      return;
    }
    else{
      let self=this;
      this.httpService.post("AdminUsers/add.json",reqData).subscribe((res:any)=>{
       if(!res.error){
         self.httpService.showSuccess(res.message);
         self.router.navigateByUrl("/"); 
       }
       /* else if(!res.error && res.data.role_id!='1'){
         self.httpService.showError("You are not allowed to access this panel Please Contact to Administration");
         console.log(res);
       } */
       else{
         self.httpService.showError(res.message);
         console.log(res);
       }
       
      });
    }

    // console.log(JSON.stringify(this.regForm.value));
  }
  ngOnInit(): void {
  }

}
