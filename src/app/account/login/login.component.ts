import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpServicesService } from 'src/app/services/http-services.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
  constructor(private router: Router,private httpService:HttpServicesService) {
    this.loginForm= new FormGroup({
      'email':new FormControl('', Validators.required),
      'password':new FormControl('', Validators.required),
      });
   }
   submitLogin(){
     let self=this;
     this.httpService.post("AdminUsers/login.json",this.loginForm.value).subscribe((res:any)=>{
      if(!res.error){
        localStorage.setItem("userTocken",res.data.jwt);
        localStorage.setItem("userInfo",JSON.stringify(res.data));
        self.httpService.showSuccess(res.message);
        this.router.navigateByUrl("/"); 
      }
      else if(!res.error && res.data.role_id!='1'){
        self.httpService.showError("You are not allowed to access this panel Please Contact to Administration");
        console.log(res);
      }
      else{
        self.httpService.showError(res.message);
        console.log(res);
      }
      
     });
   }
  ngOnInit(): void {
  }

}
